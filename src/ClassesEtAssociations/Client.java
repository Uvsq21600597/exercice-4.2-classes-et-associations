package ClassesEtAssociations;


public class Client {

	public String nom;
	private Serveur serveur;
	
	
	public Client(String nom) {
		this.nom = nom;
		this.serveur= null;
	}

	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean SeConnecter(Serveur serveur){
		
		if (serveur.Connecter(this)) {
						this.serveur=serveur;
						return true;
				}
		else return false;
				
	}
	
	public void Envoyer(String message){
	 this.serveur.Diffuser(message+"Emetteur :"+this.nom);
	 System.out.println("le client " +this.getNom()+"a envoy� ce message :"+message );
		
	}
	
	
	
	public String Recevoir(String message){
		return message;
		}
		
	
}
