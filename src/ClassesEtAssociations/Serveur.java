package ClassesEtAssociations;
import java.util.ArrayList;
import java.util.List;

public class Serveur {

	
	private List<Client> clients;
	
	
	public Serveur() {
		this.clients = new ArrayList<Client>();
	}

	
	public boolean Connecter(Client client){
		
		for ( Client c : this.clients){
					if(c.getNom().equals(client.nom)) return false;
						}
		clients.add(client);
		return true ;
		
	}


	public void Diffuser(String message){
		
		for ( Client c :this.clients){
			c.Recevoir(message);
			System.out.println("le client : "+c.getNom()+" � re�oit le message");
		}
		
	}
}
